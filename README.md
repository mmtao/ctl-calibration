# CTL Calibration.

For the MAPS top box, each wavefront sensor has an associated camera translation lens. That lens can make coarse movements 
to properly position the pupils on the wavefront sensor. The CTL also needs to make fine motions during closed loop control
to keep the pupils on the same pixels on the wavefront sensor so that downstream users of the wavefront sensor images can rely
on the changing content of the images to result from atmospheric disruption and not the motion of the light as the relative 
positions of the star, telescope, etc., change over time.

This code uses telemetry from CHAI. In that telemetry, coarse movements were made to the X and Y positions of the CTL. At the 
same time, the pupil tracker telemetered the position is believes the pupils were in. The user is left to isolate the region
of time in which this relationship is linear. Then, the mean of the ratio in the linear position is used to calibrate the number
of microns in motion in each direction on the CTL to the motion of the pupils in pixels.