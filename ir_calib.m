%% Grab data from rc3.
!scp rc3:/logs/chai2/2024-05-20-00_33_34_MST_1716190414.97477149963379.irwfs2_pupils.h5 .
fname = '2024-05-20-00_33_34_MST_1716190414.97477149963379.irwfs2_pupils.h5';
info = h5info(fname);

%% X
col_x_ts = h5read(fname, '/irwfs2_pupils/col_x_ts');
col_x = h5read(fname, '/irwfs2_pupils/col_x');
ctl_x_ts = h5read(fname, '/irwfs2_pupils/ctl_x_ts');
ctl_x = h5read(fname, '/irwfs2_pupils/ctl_x');
Ax = col_x_ts(26:80);
Ay = col_x(26:80);
Bx = ctl_x_ts(26:80);
By = ctl_x(26:80);
pixels_to_microns_x = By ./ Ay;
ax = mean(pixels_to_microns_x(3:end));
figure;
yyaxis left;
plot(Ax, Ay, 'bo-');
yyaxis right;
plot(Bx, By, 'rx-');
legend('center\_of\_light\_x', 'ctl\_x\_position');
title(sprintf('Mean of ratio in linear region is %f', ax));

%% Y
col_y_ts = h5read(fname, '/irwfs2_pupils/col_y_ts');
col_y = h5read(fname, '/irwfs2_pupils/col_y');
ctl_y_ts = h5read(fname, '/irwfs2_pupils/ctl_y_ts');
ctl_y = h5read(fname, '/irwfs2_pupils/ctl_y');
Ax = col_y_ts(170:200);
Ay = col_y(170:200);
Bx = ctl_y_ts(170:200);
By = ctl_y(170:200);
pixels_to_microns_x = By ./ Ay;
ay = mean(pixels_to_microns_x(3:end));
figure;
yyaxis left;
plot(Ax, Ay, 'bo-');
yyaxis right;
plot(Bx, By, 'rx-');
legend('center\_of\_light\_y', 'ctl\_x\_position');
title(sprintf('Mean of ratio in linear region is %f', ay));
