%% Grab data from rc3.
!scp rc3:/logs/chai2/2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5 .
info = h5info('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5');

%% X
col_x_ts = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/col_x_ts');
col_x = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/col_x');
ctl_x_ts = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/ctl_x_ts');
ctl_x = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/ctl_x');
Ax = col_x_ts(45:end-37);
Ay = col_x(45:end-37);
Bx = ctl_x_ts(38:end-37);
By = ctl_x(38:end-37);
pixels_to_microns_x = By ./ Ay;
ax = mean(pixels_to_microns_x(3:end));
figure;
yyaxis left;
plot(Ax, Ay, 'bo-');
yyaxis right;
plot(Bx, By, 'rx-');
legend('center\_of\_light\_x', 'ctl\_x\_position');
title(sprintf('Mean of ratio in linear region is %f', ax));

%% Y
col_y_ts = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/col_y_ts');
col_y = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/col_y');
ctl_y_ts = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/ctl_y_ts');
ctl_y = h5read('2024-03-22-22_18_31_MST_1711171111.84632468223572.viswfs2_pupils.h5', '/viswfs2_pupils/ctl_y');
start = 75;
Ax = col_y_ts(start:end-8);
Ay = col_y(start:end-8);
Bx = ctl_y_ts(start-7:end-8);
By = ctl_y(start-7:end-8);
pixels_to_microns_x = By ./ Ay;
ay = mean(pixels_to_microns_x(3:end));
figure;
yyaxis left;
plot(Ax, Ay, 'bo-');
yyaxis right;
plot(Bx, By, 'rx-');
legend('center\_of\_light\_y', 'ctl\_x\_position');
title(sprintf('Mean of ratio in linear region is %f', ay));
